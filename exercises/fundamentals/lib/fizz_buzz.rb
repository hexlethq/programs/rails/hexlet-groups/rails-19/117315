# frozen_string_literal: true

# BEGIN
def fizz_buzz(start, stop)
  array = []
  [*start..stop].each do |number|
    result = ''
    result += 'Fizz' if (number % 3).zero?
    result += 'Buzz' if (number % 5).zero?
    result += number.to_s if result.size.zero?
    array << result
  end
  array.join(' ')
end
# END
