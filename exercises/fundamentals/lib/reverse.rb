# frozen_string_literal: true

# BEGIN
def reverse(text)
  array = text.chars
  reversed_array = []
  reversed_array << array.pop until array.empty?
  reversed_array.join
end
# END
